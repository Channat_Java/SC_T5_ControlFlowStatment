package org.kshrd.array.array1d;

public class SignleDArrayExaple1 {

    public static void main(String[] args) {

        int array[] = new int[4];
        array[0] = 30;
        array[1] = 20;
        array[2] = 70;
        array[3] = 80;
        //array[4] = 90;

        /*for(int i=0; i<array.length; i++) {
            int arrayElement = array[i];
            System.out.println(arrayElement);
        }*/

        for(int i : array ) {
            System.out.println(i);
        }

    }


}
