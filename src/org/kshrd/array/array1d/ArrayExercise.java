package org.kshrd.array.array1d;

import java.util.Scanner;

public class ArrayExercise {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter array size = ");

        int arraySize = sc.nextInt();
        int array[] = new int[arraySize];

        // Input
        for(int i=0; i<arraySize; i++) {
            System.out.print("Enter array["+i+"] = ");
            array[i] = sc.nextInt(); // value input to array[i]
        }

        // Output
        for(int i=0; i<arraySize; i++) {
            System.out.println("Array["+i+"] = " + array[i]);
        }


        for(int arr : array) {
            System.out.println(arr);
        }






    }

}
