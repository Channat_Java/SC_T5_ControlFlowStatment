package org.kshrd.array.array2d;

public class Array2DExample1 {


    public static void main(String[] args) {

        int arrays[][] = new int[2][3]; // 2 row, 2 col


        // initialize value to array
        arrays[0][0] = 1;
        arrays[0][1] = 2;
        arrays[0][2] = 3;
        arrays[1][0] = 4;
        arrays[1][1] = 5;
        arrays[1][2] = 6;

        // -- output --
        for(int row=0; row<arrays.length; row++) {
            for(int col=0; col<arrays[row].length; col++) {
                System.out.print("Array["+row+"]["+col+"] = "+arrays[row][col]+"\t");
            }
            System.out.println();
        }


        // -> Enter row:
        // -> Enter col:
        // -> Enter array[0][0] = 23
        // -> Enter array[0][1] = 45
        // -> print value


    }



}
