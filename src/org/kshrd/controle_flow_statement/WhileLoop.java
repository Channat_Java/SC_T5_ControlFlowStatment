package org.kshrd.controle_flow_statement;

public class WhileLoop {



    public static void main(String[] args) {

        int x = 10;

        while (x<20) {
            System.out.print("Value of x is " + x);
            x++;
            System.out.print("\n");
        }
    }

}
