package org.kshrd.controle_flow_statement;

public class ForLoop {

    public static void main(String[] args) {
        for (int i=1; i<=10; i++) {
            System.out.println(i);
        }
    }

}
