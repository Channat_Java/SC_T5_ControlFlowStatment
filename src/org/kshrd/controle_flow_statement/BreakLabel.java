package org.kshrd.controle_flow_statement;

public class BreakLabel {

    public static void main(String[] args) {

        myLabel:
        for(int i=0; i<=3; i++) {
            for(int j=0; j<2; j++) {
                if (i==2 && j==1) {
                    break myLabel;
                }
                System.out.println("Row="+i+",Col="+j);
            }

        }

    }

}
