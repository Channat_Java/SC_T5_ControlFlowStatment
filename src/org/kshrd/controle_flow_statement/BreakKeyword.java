package org.kshrd.controle_flow_statement;

public class BreakKeyword {

    public static void main(String[] args) {
        int [] numbers = {10, 20, 30, 40, 50};
        for (int num : numbers) {
            if (num == 30) {
                break;
            }
            System.out.println(num);
        }
        // OR

        for (int i=0; i<numbers.length; i++) {
            int num = numbers[i];
            if (num == 30) {
                break;
            }
            System.out.println(num);
        }
    }

}
