package org.kshrd;

import java.util.Scanner;

public class Practice1 {

    public static void main(String[] args) {
        int a, b, result = 0;
        String menu;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a : ");
        a = sc.nextInt();
        System.out.print("Enter b : ");
        b = sc.nextInt();
        System.out.println("My Menu: SUM, SUB, MUL, DEV");
        System.out.print("Choose Menu: ");
        menu = sc.next();

        /*switch (menu) {
            case "SUM":
                result = a + b;
                break;
            case "SUB":
                result = a - b;
                break;
            case "MUL":
                result = a * b;
                break;
            case "DEV":
                result = a / b;
                break;
                default:
                    System.out.println("Invalid menu");
        }*/


        if (menu.equalsIgnoreCase("SUM")) {
            result = a + b;
        } else if (menu.equalsIgnoreCase("SUB")) {
            result = a - b;
        } else if (menu.equalsIgnoreCase("MUL")) {
            result = a * b;
        } else if (menu.equalsIgnoreCase("DEV")) {
            result = a / b;
        } else {
            System.out.println("Invalid menu");
        }

        System.out.println("Result is " + result);


    }


}
