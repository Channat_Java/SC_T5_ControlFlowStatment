package org.kshrd.descision_making;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

public class SwitchCase {

    public static void main(String[] args) {
        char grade;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter grade: ");
        grade = scanner.next().charAt(0);

        switch (grade) {
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B': case 'C':
                System.out.println("Well Done");
                break;
            case 'D':
                System.out.println("You pass");
                break;
            case 'E':
                System.out.println("Try more next time");
                break;
            case 'F':
                System.out.println("You fail");
                break;
            default:
                System.out.println("Invalid grade");



        }
    }
}
